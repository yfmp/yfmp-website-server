using System.Collections.Generic;
using System.Threading.Tasks;
using YfmpP.DependencyInjections;
using YfmpP.Models;

namespace YfmpP {
	public interface IHTTPConsumer {
		public Task<User> Login (string Username, string Password);
		public Task<User> Register (string Username, string Password);
		/* void Upload (string AudioFile, string Title, Tag Artist, Tag Release, Tag Album);
		   Task Play (long Id);
		   Task<AudioTrack> GetList (long Id);
		   Task<AudioTrack> GetList ();
		   */
	}
}