using System;
using YfmpP.Models;

namespace YfmpP.DependencyInjections {
	public class EncapUser : IEncapUser {

		private User User { get; set; }

		public EncapUser() {}

		public EncapUser(User User)
		{
			this.User = User;
		}

		public string GetName()
		{
			return this.User.ToString();
		}

		public User GetCurrentUser() {
			return this.User;
		}

		public void SetCurrentUser(User User) {
			this.User = User;
		}

		public bool IsPresent() {
			return null != User;
		}
	}
}