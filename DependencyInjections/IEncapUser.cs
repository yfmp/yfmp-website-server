using YfmpP.Models;

namespace YfmpP.DependencyInjections
{
	public interface IEncapUser
	{
		public string GetName();
		public User GetCurrentUser();
		public void SetCurrentUser(User user);
		public bool IsPresent();
	}
}