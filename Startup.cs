using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Collections.Generic;
using YfmpP.DependencyInjections;

namespace YfmpP
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddRazorPages();
			services.AddSingleton<IHTTPConsumer, HTTPConsumer>();
			services.AddSingleton<IEncapUser, EncapUser>();
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			app.UseDeveloperExceptionPage();
			/* app.UseDatabaseErrorPage(); */
			// The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.

			/* app.UseHttpsRedirection(); */
			app.UseStaticFiles();

			app.UseRouting();

			// app.UseAuthentication();
			// app.UseAuthorization();

			app.UseEndpoints(endpoints => {
				endpoints.MapRazorPages();
			});
		}
	}
}
