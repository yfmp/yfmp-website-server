﻿using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections;
using System.Net.Http;
using System.Net.Sockets;
using System.Threading.Tasks;
using System;
using YfmpP.DependencyInjections;
using YfmpP.Models;

namespace YfmpP.Pages
{
	public class IndexModel : PageModel
	{

		public string Username{get; set;}
		public string Password{get; set;}
		public IHTTPConsumer consumer{get; set;}
		public string Error{get; set;}

		public IEncapUser User { get; set; }

		public IndexModel(IHTTPConsumer consumer, IEncapUser User) {
			this.consumer = consumer;
			this.User = User;
		}

		public IActionResult OnPost() {
			this.Username = Request.Form["Username"];
			this.Password = Request.Form["Password"];

			try {
				var task = consumer.Login(this.Username, this.Password);
				task.Wait();
				User user = task.Result;

				User.SetCurrentUser(user);

				this.Error = "You have successfully logged in";

				return RedirectToPage("/Upload");

			} catch(AggregateException ae) {
				ae.Handle((e) => {
					if(e is HttpRequestException) {
						if(e.InnerException is SocketException) {
							this.Error = "Could not connect to application server; try again later";
							return true;
						}
						this.Error = "Wrong login or password";
						return true;
					}
					return false;
				});

				return Page();
			}
		}
	}
}
