using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc;  
using System;
using YfmpP.DependencyInjections;
using YfmpP.Models;

namespace YfmpP.Pages
{
	public class RegisterModel : PageModel
	{
		public string Username{get; set;}
		public string Password{get; set;}
		public string PasswordConf{get; set;}
		public string Error{get; set;}

		private IHTTPConsumer consumer;

		public RegisterModel(IHTTPConsumer consumer)
		{
			this.consumer = consumer;
		}

		public void Register(string Username, string Password)
		{
			consumer.Register(Username, Password);
		}

		public IActionResult OnPost()
		{
			this.Username = Request.Form["Username"];
			this.Password = Request.Form["Password"];
			this.PasswordConf = Request.Form["PasswordConf"];

			if(this.Password.Equals(this.PasswordConf)) {
				try {
					var task = consumer.Register(this.Username, this.Password);
					task.Wait();
					User newUser = task.Result;

					return RedirectToPage("/Index");
				} catch(Exception e) {
					this.Error = "Something went wrong: " + e;
					Console.WriteLine("QEWQWEQWE");
				}
			} else {
				this.Error = "Passwords did not match please retype it.";
			}

			return Page();
		}

	}
}