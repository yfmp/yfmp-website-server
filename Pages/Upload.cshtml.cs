using Microsoft.AspNetCore.Mvc.RazorPages; 
using Microsoft.AspNetCore.Mvc;
using System;
using YfmpP.DependencyInjections;

namespace YfmpP.Pages
{
	public class UploadModel : PageModel
	{
		private IEncapUser User;

		public UploadModel(IEncapUser User) {
			this.User = User;
		}

		public IActionResult OnGet() {
			if(User.IsPresent())
				return Page();
			return RedirectToPage("/Index");
		}
	}
}