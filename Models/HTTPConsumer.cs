using System.Collections.Generic;
using System.Net.Http;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;
using System;
using YfmpP.DependencyInjections;
using YfmpP.Models;

namespace YfmpP {
	public class HTTPConsumer : IHTTPConsumer {

		static readonly HttpClient client = new HttpClient();

		public async Task<User> Register(string Username, string Password) {
			var response = await client.GetStringAsync(
					$"http://localhost:8080/users/register?name={Username}&password={Password}");
			return JsonSerializer.Deserialize<User>(response);
		}

		public async Task<User> Login(string Username, string Password) {
			var response = await client.GetStringAsync(
					$"http://localhost:8080/users/login?login={Username}&password={Password}");
			return JsonSerializer.Deserialize<User>(response);
		}

		/* public async void Upload(string AudioFile, string Title, Tag Artist, Tag Album, Tag Release) { */

		/* } */
	}
}