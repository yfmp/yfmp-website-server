namespace YfmpP.Models {
	public class Tag {
		private long id { get; set; }
		private string tag { get; set; }

		public Tag () { }

		public Tag (string tag) {
			this.tag = tag;
		}

	}

}