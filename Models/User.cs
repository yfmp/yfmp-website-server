namespace YfmpP.Models {
	public class User {

		private long id { get; set; }
		private string username { get; set; }

		public User () {}

		public User (string username) {
			this.username = username;
		}
	}

}