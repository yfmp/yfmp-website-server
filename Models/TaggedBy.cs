namespace YfmpP.Models {
	public class TaggedBy {
		private long id { get; set; }
		private User user { get; set; }
		private AudioTrack track { get; set; }
		private Tag tag { get; set; }

		public TaggedBy () {}

		public TaggedBy (User user, AudioTrack track, Tag tag) {
			this.user = user;
			this.track = track;
			this.tag = tag;
		}

	}

}