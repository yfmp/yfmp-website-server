namespace YfmpP.Models {
	public class AudioTrack  
	{
		private long id { get; set; }
		private string title { get; set; }
		private Tag artist { get; set; }
		private Tag album { get; set; }
		private Tag release { get; set; }
		private string path { get; set; }
	}

}